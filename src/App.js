import React from 'react';
import './App.scss';
import marked from 'marked';

const initialState = "# This is the header\n" +
    "## This is the sub header\n" +
    "[I'm an link](https://www.google.com)\n" +
    "`` This is the inline-code ``\n" +
    "```\n" +
    "Code block\n" +
    "```\n" +
    "1. The first item\n" +
    "2. The second item\n" +
    "> This is the blockquote\n" +
    "![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png \"Logo Title Text 1\")\n" +
    "**This is the bolded text**"

marked.setOptions({
    breaks: true
})

class Markdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            outputText: initialState
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let outputText = event.target.value;
        this.setState({
            outputText: outputText
        });
    }

    render() {
        return (
            <div className="markdown-container d-flex flex-column">
                <div className="py-1 mb-3 py-md-3 py-lg-3 shadow
                                d-flex justify-content-center align-items-center align-content-center">

                    <strong>
                        <p className="markdown-header text-center m-0"> React JS Markdown Preview </p>
                    </strong>

                </div>

                <div className="px-2 py-lg-5 d-xl-flex flex-xl-row">


                        <div className="py-lg-3 d-flex flex-column col-xl-6">

                            <strong><p className="textarea-header"> Markdown </p></strong>

                            <textarea className="form-control shadow p-3 mb-5 bg-white rounded markdown" id="editor"
                                      rows="14" value={this.state.outputText} onChange={this.handleChange}>
                        </textarea>
                        </div>

                        <div className="py-lg-3 px-2 d-flex flex-column col-xl-6">

                            <strong><p className="textarea-header"> Preview </p></strong>

                            <div id="preview"  className="markdown-preview py-1 px-2 border border-secondary"
                                 dangerouslySetInnerHTML={{ __html: marked(this.state.outputText) }}>
                            </div>
                        </div>


                </div>

                <footer className="d-flex text-center footer-markdown justify-content-center mt-3">
                    © 2020 FreeCodeCamp - Iyemay González Vargas -
                </footer>

            </div>

        );
    }
}

export default Markdown;
